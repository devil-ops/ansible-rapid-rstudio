#!/bin/bash
#
# Install Singularity
#
DEBIAN_FRONTEND=noninteractive
WORKING_DIR=/srv/singularity-images
sudo apt-get update
sudo apt-get install -yq --no-install-recommends \
     apt-transport-https \
     build-essential \
     bzip2 \
     ca-certificates \
     curl \
     golang-1.16-go \
     httpie \
     libarchive-dev \
     libgpgme11-dev \
     libjpeg-dev \
     libpng-dev \
     libseccomp-dev \
     libssl-dev \
     locales \
     net-tools \
     pkg-config
     pwgen \
     software-properties-common \
     squashfs-tools \
     unzip \
     uuid-dev \
     wget \
     zlib1g-dev
sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get autoremove -y
sudo update-alternatives --install /usr/bin/go go /usr/lib/go-1.16/bin/go 0

if [ -d "$WORKING_DIR" ]; then rm -Rf $WORKING_DIR; fi
mkdir -p $WORKING_DIR

# Install a recent Singularity version
# This is done in a subshell to keep the working directory neutral
( sudo go get -u github.com/golang/dep/cmd/dep
export VERSION=3.8.1 && # adjust this as necessary \
    sudo mkdir -p /usr/local/src/github.com/sylabs && \
    cd /usr/local/src/github.com/sylabs && \
    sudo wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
    sudo tar -xzf singularity-ce-${VERSION}.tar.gz && \
    cd ./singularity-ce-${VERSION} && \
    sudo ./mconfig && \
    sudo make -C ./builddir && \
    sudo make -C ./builddir install
)

# need AllowTcpForwarding to be true for ssh port forwarding to work
sudo sed -i 's/#AllowTcpForwarding yes/AllowTcpForwarding yes/' /etc/ssh/sshd_config
sudo service ssh restart

# grab the pre-built singularity image
singularity pull \
     $WORKING_DIR/singularity-rstudio.sif \
     oras://gitlab-registry.oit.duke.edu/devil-ops/singularity-rstudio/singularity-rstudio:0.0.1

# put the source somewhere so users can modify things after the deploy
sudo git clone https://gitlab.oit.duke.edu/devil-ops/ansible-rapid-rstudio.git $WORKING_DIR/src/deploy
sudo git clone https://gitlab.oit.duke.edu/devil-ops/singularity-rstudio.git $WORKING_DIR/src/singularity-rstudio

sudo cp $WORKING_DIR/src/singularity-rstudio/singularity-scripts/* $WORKING_DIR/
sudo cp $WORKING_DIR/src/singularity-rstudio/singularity-scripts/* /etc/skel

# drop off some documentation in their homedir
sudo cp -R $WORKING_DIR/src/deploy/singularity-user-docs/* /etc/skel

# setup this beautiful homedir for existing users we care about
./update_existing_users.sh

#
# leave a flag saying we got done
#
echo "Rstudio app install succeeded" > /tmp/rapid_image_complete
