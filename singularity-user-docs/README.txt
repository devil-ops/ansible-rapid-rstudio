RStudio is packaged in a Singularity container so that the binary is easily
portable to any platform that has Singularity installed.

To run an instance of RStudio, run these commands

     cd ~ 
     ./run-rstudio

then follow the instructions displayed once the run-rstudio script starts
to enable ssh port forwarding to access your container.

Once the container is running you will have access to files in your home directory
and /tmp on the server from your web browser, so you should place your data files
ion your home directory.

=========
If you want to modify the RStudio container a local copy of the Singularity 
build script is stored on this server here:

   /srv/singularity-images/src/singularity-rstudio
